﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Timers;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public TextMeshProUGUI timerText;    
    public GameObject lostTextObject;
    bool isGrounded = true;
    bool isStuck = false;
    bool hasWon = false;
    bool hasLost = false;
    ArrayList groundedList = new ArrayList(new string[] {"Ground"});

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
        lostTextObject.SetActive(false);
    }

    void Update()
    {
        float currentTime = Time.time;
        if(hasWon == false && hasLost == false){
            timerText.text = "Time remaining: " + (80 - currentTime).ToString("f2");
        }
        if(currentTime >= 80 && hasWon == false){
            hasLost = true;
            lostTextObject.SetActive(true);
        }
    }

    void OnFire()
    {
        if(isGrounded == true && isStuck == false){
            rb.AddForce(0f, 430f, 0f);
        }
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 17 && hasLost == false)
        {
            hasWon = true;
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
        else if(other.gameObject.CompareTag("Trap"))
        {
            isStuck = true;
            speed = speed / 4;
            rb.velocity = new Vector3(0, 0, 0);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Trap"))
        {
            isStuck = false;
            speed = speed * 4;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (groundedList.Contains(col.gameObject.name)){
            isGrounded = true;
        }
    }
 
    void OnCollisionExit(Collision col)
    {
        if (groundedList.Contains(col.gameObject.name)){
            isGrounded = false;
        }
    }
}
